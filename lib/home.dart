import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'RequestDataFromAPI.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController CEP = TextEditingController();
  String _logradouro = "";
  String _complemento = "";
  String _bairro = "";
  String _localidade = "";
  String _uf = "";
  String _ibge = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("VIA CEP"),
        backgroundColor: Colors.redAccent,
      ),
      body: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red, width: 5.0),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(32),
                child: TextField(
                  controller: this.CEP,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: "Digite um CEP",
                  ),
                  maxLength: 8,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.green,
                  ),
                ),
              ),
              RaisedButton(
                child: Text("Consultar"),
                onPressed: () {
                  RequestDataFromAPI(cep: CEP.text).getCEP().then((d) {
                    setState(() {
                      _logradouro = (d['logradouro'] != "") ? d['logradouro'] : "Endereço Não Encontrado!";
                      _complemento = (d['complemento'] != "") ? d['complemento'] : "Sem Complemento!";
                      _bairro = (d['bairro'] != "") ? d['bairro'] : "Bairro Não Encontrado!";
                      _localidade = (d['localidade'] != "") ? d['localidade'] : "Cidade Não Encontrada!";
                      _uf = (d['uf'] != "") ? d['uf'] : "UF Não Encontrado!";
                      _ibge = (d['ibge'] != "") ? d['ibge'] : "IBGE Não Encontrado!";
                    });
                  });
                },
              ),
              Text("Logradouro: ${_logradouro}"),
              Text("Complemento: ${_complemento}"),
              Text("Bairro: ${_bairro}"),
              Text("Localidade: ${_localidade}"),
              Text("UF: ${_uf}"),
              Text("IBGE: ${_ibge}"),
            ],
          ),
        ),
      ),
    );
  }
}
