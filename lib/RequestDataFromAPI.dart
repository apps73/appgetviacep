import 'dart:convert';
import 'package:http/http.dart' as http;

class RequestDataFromAPI {
  String cep;
  RequestDataFromAPI({this.cep});
  getCEP() async {
    String CEPDigitado = cep;
    String url = "http://viacep.com.br/ws/${CEPDigitado}/json/";
    http.Response response;
    response = await http.get(url);
    Map<String, dynamic> jsonData = json.decode(response.body);
    return jsonData;
  }
}


